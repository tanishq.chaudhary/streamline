# StreamLine

A simple river-crossing game built in python using pygame.

# Controls

Both the player move with the arrow keys.


# Specifications

*  Its a two player game
*  Only one player is active at any given point of time
*  There are a total of 3 rounds
*  The difficulty increases, if the player succeds in crossing the river
*  There are fixed obstacles and moving obstacles.
*  If the player is on one of the partition, they are unaffected by the moving enemies
*  The player is also safe within certain limits of the partitions
*  Touching any fixed obstacle results in an instant death