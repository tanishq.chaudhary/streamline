from time import sleep
import pygame
from pygame import *
from configurations import BROWN, YELLOW, CYAN, bigFont, BLACK, death_texts, success_texts, smallFont, GREEN, RED
import sys
import random
import math

# starting the pygame module

pygame.init()

# setting up the window with dimensions and the name of the game
WIDTH, HEIGHT = 1280, 720
DISPLAY = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('StreamLine')

# loading images
spaceshipImage = pygame.image.load('images/spaceship.png')
stoneImage = pygame.image.load('images/stone.png')
sharkImage = pygame.image.load('images/fish.png')
pirateImage = pygame.image.load('images/pirate.png')
whirlpoolImage = pygame.image.load('images/whirlpool.png')
invertedSpaceshipImage = pygame.image.load('images/invertedSpaceship.png')

# clock set up; fps is 60
FPS = 60
# setting up the partitions as they remain the same
# each tuple is stored as the start x and the end x
partitions = [(130, 170), (235, 275), (340, 380), (445, 485), (550, 590)]


def player():
    """ This function creates a general template for the player
    """
    return {
        'score': 100,
        'round': 1,
        'initialPosition': [],
        'death': False,
        'presentPosition': [],
        'reached': False
    }


def generateFixedEnemies(player):
    """ We generate the enemies according to the player's level
    """
    numberFixedEnemies = player['round'] * 10
    fixedEnemies = []

    for i in range(numberFixedEnemies):
        x = random.randrange(0, WIDTH)
        y = random.randrange(0, 600)
        y += 60
        if y + 64 > 600:
            y = 600 - 64
        fixedEnemies.append([random.choice([whirlpoolImage, stoneImage]), x, y])

    return fixedEnemies


def generateMovingEnemies(player):
    """ We generate the enemies according to the player's level
    """
    base = 5
    numberMovingEnemies = base * 2 if player['round'] > 1 else base
    movingEnemies = []

    for i in range(base):
        x = random.randrange(0, WIDTH)
        y = partitions[i][0] - 70

        movingEnemies.append([random.choice([sharkImage, pirateImage]), x, y, random.choice(['r', 'l'])])

    if numberMovingEnemies == base * 2:
        for i in range(base):
            x = random.randrange(0, WIDTH)
            y = partitions[i][0] - 70

            movingEnemies.append([random.choice([sharkImage, pirateImage]), x, y, random.choice(['r', 'l'])])

    return movingEnemies


def updateMovingEnemies(player, movingEnemies):
    """
    Updates the moving enemies according to the player's level
    """
    speed = math.sqrt(player['round']) * 10

    for i in range(len(movingEnemies)):
        x = movingEnemies[i][1]
        y = movingEnemies[i][2]
        direction = movingEnemies[i][3]

        if direction == 'r':
            if x + speed > WIDTH:
                direction = 'l'
        if direction == 'l':
            if x - speed < 0:
                direction = 'r'

        if direction == 'r':
            x += speed
        else:
            x -= speed

        movingEnemies[i][1] = x
        movingEnemies[i][2] = y
        movingEnemies[i][3] = direction

    return movingEnemies


def score(player, fixedEnemies, movingEnemies, playerActivated):
    """ We calculate the score according to the player positions
    """
    # oldScore = player['score']
    temp = 0

    if not playerActivated:
        for i in fixedEnemies:
            if i[2] < player['presentPosition'][1]:
                temp += 5
        for i in movingEnemies:
            if i[2] < player['presentPosition'][1]:
                temp += 10
    else:
        for i in fixedEnemies:
            if i[2] > player['presentPosition'][1]:
                temp += 5
        for i in movingEnemies:
            if i[2] > player['presentPosition'][1]:
                temp += 10

    return temp


def collisionDetection(player, fixedEnemies, movingEnemies):
    """
    We detect if the blocks have collided or not
    """
    player_x = player['presentPosition'][0]
    player_y = player['presentPosition'][1]

    for i in fixedEnemies:
        enemy_x = i[1]
        enemy_y = i[2]
        if (player_x <= enemy_x < player_x + 64) or (enemy_x <= player_x < enemy_x + 64):
            if (player_y <= enemy_y < player_y + 64) or (enemy_y <= player_y < enemy_y + 64):
                return True

    for i in movingEnemies:
        enemy_x = i[1]
        enemy_y = i[2]
        if (player_x <= enemy_x < player_x + 64) or (enemy_x <= player_x < enemy_x + 64):
            if (player_y <= enemy_y < player_y + 64) or (enemy_y <= player_y < enemy_y + 64):
                return True

    return False


# initializing the game variables
player1 = player()
player2 = player()
player1['initialPosition'] = [WIDTH / 2 - 32, HEIGHT - 32]
player2['initialPosition'] = [WIDTH / 2 - 32, 32]
player1['presentPosition'] = player1['initialPosition']
player2['presentPosition'] = player2['initialPosition']

playerCurrent = player1
playerActivated = True
fixedEnemies = generateFixedEnemies(playerCurrent)
movingEnemies = generateMovingEnemies(playerCurrent)

totalCycles = 0
tempScore = 0
timeTaken = 0
loopCounter = 0
gameFinished = False

# the main game loop
while not gameFinished:

    # setting up the background display
    DISPLAY.fill(CYAN)
    pygame.draw.rect(DISPLAY, YELLOW, (0, 0, WIDTH, 60), 0)
    pygame.draw.rect(DISPLAY, YELLOW, (0, HEIGHT - 60, WIDTH, 60), 0)
    for i in partitions:
        pygame.draw.rect(DISPLAY, BROWN, (0, i[0], WIDTH, 40), 0)

    # storing the score till now
    tempScore = max(tempScore, score(playerCurrent, fixedEnemies, movingEnemies, playerActivated))

    loopCounter += 1
    if loopCounter % FPS == 0:
        loopCounter = 0
        timeTaken += 1

    # checking which player is going to play right now
    if playerCurrent['death'] or playerCurrent['reached']:
        if playerCurrent['reached']:
            playerCurrent['round'] += 1

        playerCurrent['score'] = playerCurrent['score'] + tempScore - timeTaken
        sleep(2)
        if not playerActivated:
            totalCycles += 1
            if totalCycles == 3:
                break
            playerCurrent = player1
            playerActivated = True
        else:
            playerCurrent = player2
            playerActivated = False

        # resetting the values, all except for the score
        playerCurrent['reached'] = False
        playerCurrent['death'] = False
        tempScore = 0
        timeTaken = 0
        playerCurrent['presentPosition'] = playerCurrent['initialPosition']
        # drawing the enemies for the new player
        fixedEnemies = generateFixedEnemies(playerCurrent)
        movingEnemies = generateMovingEnemies(playerCurrent)

    # event handling for player's movements

    for event in pygame.event.get():
        # checking if the 'x' button is pressed
        if event.type == QUIT:
            game_finished = True
            pygame.quit()
            sys.exit()

        # changing the positions of the player according to the inputs recieved
        x_change, y_change = 0, 0
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                x_change -= 10
            elif event.key == pygame.K_RIGHT:
                x_change += 10
            elif event.key == pygame.K_UP:
                y_change -= 10
            elif event.key == pygame.K_DOWN:
                y_change += 10

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                x_change = y_change = 0

    playerCurrent['presentPosition'][0] += x_change
    playerCurrent['presentPosition'][1] += y_change

    # detecting out-of-screen conditions
    x, y = playerCurrent['presentPosition']
    if x < 0:
        x = 0
    if x > WIDTH - 64:
        x = WIDTH - 64
    if y < 0:
        y = 0
    if y > HEIGHT - 64:
        y = HEIGHT - 64
    playerCurrent['presentPosition'] = [x, y]

    # drawing the enemies
    for i in fixedEnemies:
        DISPLAY.blit(i[0], (i[1], i[2]))
    # updating the enemy positions
    updateMovingEnemies(playerCurrent, movingEnemies)
    for i in movingEnemies:
        DISPLAY.blit(i[0], (i[1], i[2]))

    # implementing collision detection
    playerCurrent['death'] = True if collisionDetection(playerCurrent, fixedEnemies, movingEnemies) else False

    # the player is saved if on one of the partitions or in the start box or the end box 
    if playerCurrent['death']:
        for i in partitions:
            if i[0] <= playerCurrent['presentPosition'][1] + 32 <= i[1]:
                playerCurrent['death'] = False
        if playerCurrent['presentPosition'][1] > HEIGHT - 60 or playerCurrent['presentPosition'][1] < 60:
            playerCurrent['death'] = False

    # implementing level completion
    if playerActivated:
        if playerCurrent['presentPosition'][1] <= 32:
            playerCurrent['reached'] = True
    if not playerActivated:
        if playerCurrent['presentPosition'][1] >= HEIGHT - 60 - 32:
            playerCurrent['reached'] = True

    # drawing the player
    if playerActivated:
        DISPLAY.blit(spaceshipImage, playerCurrent['presentPosition'])
    else:
        DISPLAY.blit(invertedSpaceshipImage, playerCurrent['presentPosition'])

    # writing the scores onto the screen
    if playerActivated:
        score1 = playerCurrent['score'] + tempScore - timeTaken
        score2 = player2['score']
    else:
        score1 = player1['score']
        score2 = playerCurrent['score'] + tempScore - timeTaken

    score1 = str(score1)
    score2 = str(score2)
    DISPLAY.blit(bigFont.render("SCORE: ", True, BLACK), (WIDTH - WIDTH // 4, HEIGHT - 45))
    DISPLAY.blit(bigFont.render(score1, True, BLACK), (WIDTH - WIDTH // 8, HEIGHT - 45))
    DISPLAY.blit(bigFont.render("SCORE: ", True, BLACK), (WIDTH - WIDTH // 4, 10))
    DISPLAY.blit(bigFont.render(score2, True, BLACK), (WIDTH - WIDTH // 8, 10))

    if playerCurrent['death']:
        DISPLAY.blit(bigFont.render(death_texts[0], True, RED), (WIDTH // 2 - 45, HEIGHT // 2 - 20))
        DISPLAY.blit(smallFont.render(death_texts[1], True, BLACK), (WIDTH // 2 - 75, HEIGHT // 2 + 12))
    elif playerCurrent['reached']:
        DISPLAY.blit(bigFont.render(success_texts[0], True, GREEN), (WIDTH // 2 - 45, HEIGHT // 2 - 20))
        DISPLAY.blit(smallFont.render(success_texts[1], True, BLACK), (WIDTH // 2 - 65, HEIGHT // 2 + 12))

    # updation of the screen
    display.update()
    time.Clock().tick(FPS)

while True:
    DISPLAY.fill(CYAN)
    if player1['score'] > player2['score']:
        DISPLAY.blit(bigFont.render("PLAYER 1 WINS!", True, BLACK), (WIDTH // 2 - 100, HEIGHT // 2))
    elif player1['score'] < player2['score']:
        DISPLAY.blit(bigFont.render("PLAYER 2 WINS!", True, BLACK), (WIDTH // 2 - 100, HEIGHT // 2))
    else:
        DISPLAY.blit(bigFont.render("IT'S A DRAW!", True, BLACK), (WIDTH // 2 - 80, HEIGHT // 2))
    display.update()
    sleep(2)
    break
quit()
