import pygame
from pygame import *

pygame.init()

# colors set up
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BROWN = (150, 75, 0)
CYAN = (75, 220, 255)
YELLOW = (220, 255, 65)

# text variations
bigFont = pygame.font.Font('freesansbold.ttf', 32, bold=True, italic=False)
smallFont = pygame.font.Font('freesansbold.ttf', 24, bold=False, italic=True)

# display texts
death_texts = ["DEATH!", "By Sir Obstacle"]
success_texts = ["REACHED", "Road To Victory!"]
